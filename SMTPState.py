class SMTPState:
    def __init__(self):
        self.reset_state()
        self.tls_active = False

    def reset_state(self):
        self.in_mail = False
        self.in_rcpt = False
        self.quit = False

    def reset_tls(self):
        self.tls_active = False

    def set_in_mail(self):
        self.in_mail = True

    def set_quit(self):
        self.quit = True

    def set_in_rcpt(self):
        self.in_rcpt = True

    def set_tls_active(self):
        self.tls_active = True