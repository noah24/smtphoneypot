import socket
import threading
import sys
from SMTPConnection import SMTPConnection


class Server:

    log_file = 'test.txt'

    def __init__(self, host_name):
        self.write_lock = threading.Lock()
        self.thread_list = []
        self.name = host_name


    def listen_forever(self):
        sock = socket.socket()
        sock.bind(('localhost', 5000))
        sock.listen(3)

        try:
            while True:
                # pylint: disable=unused-variable
                conn, addr = sock.accept()
                conn.settimeout(10.0)
                smtp_connection_thread = SMTPConnection(self, conn)
                smtp_connection_thread.daemon = True
                self.thread_list.append(smtp_connection_thread)
                smtp_connection_thread.start()

        except KeyboardInterrupt:
            print('interrupt received')
            for smtp_connection_thread in self.thread_list:
                smtp_connection_thread.join()
            sock.shutdown(socket.SHUT_RDWR)
            sock.close()

            sys.exit(0)
