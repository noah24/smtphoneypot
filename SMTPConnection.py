import threading
import time
import re
import ssl
import socket
from ssl import Purpose
from SMTPState import SMTPState


class SMTPConnection(threading.Thread):
    def __init__(self, parent, conn):
        super(SMTPConnection, self).__init__()

        self.parent = parent
        self.conn = conn
        self.transcript = ""
        self.send_buffer = ""
        self.state = SMTPState()

        self.command_table = {
            "helo" : self.helo_cmd,
            "ehlo" : self.ehlo_cmd,
            "rset" : self.rset_cmd,
            "mail" : self.mail_cmd,
            "rcpt" : self.rcpt_cmd,
            "quit" : self.quit_cmd,
            "vrfy" : self.vrfy_cmd,
            "data" : self.data_cmd,
            "auth" : self.auth_cmd,
            "starttls" : self.starttls_cmd
        }


    def introduction(self):
        self.send("220 " + self.parent.name + " ESMTP Postfix")

    def unknown_cmd_response(self):
        self.send("502 5.5.2 Error: command not recognized")

    def rset_cmd(self):
        if (len(self.argv) != 1):
            self.send("501 5.5.4 Syntax: RSET")
            return

        self.state.reset_state()
        self.send("250 2.1.0 Ok")

    def helo_cmd(self):
        if (len(self.argv) == 1):
            self.send("501 Syntax: HELO hostname")
            return

        self.state.reset_state()
        self.send("250 " + self.parent.name)

    def ehlo_cmd(self):
        if (len(self.argv) == 1):
            self.send("501 Syntax: EHLO hostname")
            return

        self.state.reset_state()
        feature_list = [self.parent.name, "PIPELINING", "SIZE 10240000",
                        "VRFY", "ETRN", "AUTH LOGIN PLAIN", "STARTTLS",
                        "ENHANCEDSTATUSCODES", "8BITMIME"]

        for feature in feature_list:
            self.buffer("250-" + feature)
        self.buffer("250 DSN")
        self.flush()

    def syntax_checks(self, tofrom: str) -> bool:
        # Valid syntax can come in one or two tokens.
        # This takes care of mail from: <xyz> and mail from:<xyz>
        arg = ""
        num_tokens = 0
        try:
            arg += self.argv[1].strip("\r\n")
            num_tokens += 1
            if (re.match("^" + tofrom + ":$", arg)):
                arg += self.argv[2].strip("\r\n")
                num_tokens += 1
        except IndexError:
            pass

        if (not re.match("^(" + tofrom + ":)(.+)", arg)):
            syntax = "MAIL FROM" if tofrom == "from" else "RCPT TO"
            self.send("501 5.5.4 Syntax: " + syntax + ":<address>")
            return False

        # TODO: technically, we need to check for < ... >

        if (len(self.argv) - num_tokens > 1):
            self.send("555 5.5.4 Unsupported option: "
                      + self.argv[num_tokens + 1].strip("\r\n"))
            return False

        return True

    def mail_cmd(self):
        if (self.state.in_mail):
            self.send("503 5.5.1 Error: nested MAIL command")
            return

        if (self.syntax_checks("from")):
            self.state.set_in_mail()
            self.send("250 2.1.0 Ok")

    def rcpt_cmd(self):
        if (not self.state.in_mail):
            self.send("503 5.5.1 Error: need MAIL command")
            return

        if (self.syntax_checks("to")):
            self.state.set_in_rcpt()
            self.send("250 2.1.5 Ok")

    def data_cmd(self):
        if (not self.state.in_rcpt):
            self.send("503 5.5.1 Error: need RCPT command")
            return

        if (len(self.argv) != 1):
            self.send("501 5.5.4 Syntax: DATA")
            return

        self.send("354 End data with <CR><LF>.<CR><LF>")

        while True:
            data = self.conn.recv(4096)
            data_string = str(data, 'utf8', 'ignore')

            self.transcript += data_string

            if (not data
                or data_string.endswith("\r\n.\r\n")
                or data_string.strip("\r\n") == "."):
                break

        # TODO: 250 2.0.0 Ok: queued as QUEUE_ID
        self.send("250 2.0.0 Ok")
        self.state.reset_state()

    def vrfy_cmd(self):
        if (len(self.argv) == 1):
            self.send("501 5.5.4 Syntax: VRFY address")
            return

        if (len(self.argv) > 2):
            self.send("501 5.1.3 Bad recipient address syntax")
            return

        self.send("252 2.0.0 " + self.argv[1].strip("\r\n"))

    def quit_cmd(self):
        self.send("221 2.0.0 Bye")
        self.state.set_quit()

    def auth_cmd(self):
        if (self.state.in_mail):
            self.send("503 5.5.1 Error: MAIL transaction in progress")
            return

        if (len(self.argv) < 2 or len(self.argv) > 3):
            self.send("501 5.5.4 Syntax: AUTH mechanism")
            return

        mechanisms = ["plain", "login"]
        mechanism = self.argv[1].strip("\r\n")
        initial_response = self.argv[2] if len(self.argv) == 3 else None

        if (mechanism not in mechanisms):
            self.send("535 5.7.8 Error: authentication failed: " \
                      "Invalid authentication mechanism")
            return

        mechanism_handlers = {
            "plain" : self.auth_plain_mechanism,
            "login" : self.auth_login_mechanism
        }

        mechanism_handlers[mechanism](initial_response)

    def auth_plain_mechanism(self, initial_response):
        if (not initial_response):
            self.send("334 ")
            client_response = self.conn.recv(4096)
            response_string = str(client_response, 'utf8', 'ignore')
            self.transcript += response_string

            if (response_string.strip("\r\n") == "*"):
                self.send("501 5.7.0 Authentication aborted")
                return

        self.send("535 5.7.8 Error: authentication failed:")

        # we want to write b64 decoded credentials out to a separate file

    def auth_login_mechanism(self, initial_response):
        if (not initial_response):
            self.send("334 VXNlcm5hbWU6")
            client_response = self.conn.recv(4096)
            response_string = str(client_response, 'utf8', 'ignore')
            self.transcript += response_string

            if (response_string.strip("\r\n") == "*"):
                self.send("501 5.7.0 Authentication aborted")
                return

        self.send("334 UGFzc3dvcmQ6")
        client_response = self.conn.recv(4096)
        response_string = str(client_response, 'utf8', 'ignore')
        self.transcript += response_string
        if (response_string.strip("\r\n") == "*"):
            self.send("501 5.7.0 Authentication aborted")
        else:
            self.send("535 5.7.8 Error: authentication failed:")

        # Behaviour can be made more realistic by adding auth username
        # to smtp state machine

    def starttls_cmd(self):
        if (len(self.argv) != 1):
            self.send("501 5.5.4 Syntax: STARTTLS")
            return

        if (self.state.tls_active):
            self.send("554 5.5.1 Error: TLS already active")
            return

        self.send("220 2.0.0 Ready to start TLS")

        context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        context.load_cert_chain("ssl-cert-snakeoil.pem",
                                "ssl-cert-snakeoil.key")
        self.conn = context.wrap_socket(self.conn, server_side=True)

        self.state.reset_state()
        self.state.set_tls_active()

    def buffer(self, line):
        self.send_buffer += line + "\r\n"

    def send(self, line):
        self.send_buffer = line + "\r\n"
        self.flush()

    def flush(self):
        self.conn.send(self.send_buffer.encode())
        self.send_buffer = ""

    def log_transcript(self):
        while self.parent.write_lock.locked():
            time.sleep(0.01)

        self.parent.write_lock.acquire()

        with open(self.parent.log_file, "a+") as f:
            f.write("BEGIN  TRANSCRIPT\n")
            f.write(self.transcript)
            f.write("END OF TRANSCRIPT\n\n\n")
        f.close()

        self.parent.write_lock.release()


    def run(self):
        try:
            self.introduction()

            while True:
                raw_command = self.conn.recv(4096)
                raw_string = str(raw_command, 'utf8', 'ignore')
                lower_string = raw_string.lower()
                self.argv = lower_string.split(' ')
                command = self.argv[0].strip("\r\n ")

                # record the input
                self.transcript += raw_string

                try:
                    self.command_table[command]()
                except KeyError:
                    self.unknown_cmd_response()

                if (not raw_command or self.state.quit):
                    break

        except Exception as e:
            print("Exception: ", e)

        finally:
            try:
                if (self.state.tls_active):
                    self.conn = self.conn.unwrap()
                self.conn.shutdown(socket.SHUT_RDWR)
            except Exception as e:
                print("Shutdown Exception: ", e)                
            finally:
                self.conn.close()
            self.log_transcript()
            self.parent.thread_list.remove(threading.current_thread())